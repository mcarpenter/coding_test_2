Gem::Specification.new do |s|
  s.name          = "soccer_standings"
  s.version       = "0.0.1"
  s.description   = "Calculates soccer league standings"
  s.summary       = "Soccer Standings"
  s.platform      = Gem::Platform::RUBY
  s.authors       = ["Matt Carpenter"]
  s.email         = ["mattcarpenter at gmail.com"]
  
  s.executables   = ["soccer_standings"]
  s.files         = `git ls-files`.split("\n")
  s.require_paths = ["lib"]
end