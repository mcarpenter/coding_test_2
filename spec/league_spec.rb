require 'spec_helper.rb'
module SoccerStandings
  describe League do
    before :each do
      games = [Game.new("Seahawks 58, Cardinals 0"),
               Game.new("49ers 31, Seahawks 42"),
               Game.new("Seahawks 24, Patriots 23"),
               Game.new("Rams 19, Seahawks 13"),
               Game.new("49ers 3, Rams 3"),
               Game.new("Bears 8, Raiders 0"),
               Game.new("Apples 1, Cats 1"),
               Game.new("Apples 2, Cats 2")]
      @teams = Team::from_game_array(games)
      @teams = League::sort_teams_by_standings(@teams)
    end

    describe '#calculate_team_standings' do
      it "should calculate 3 points per win" do
        @teams.select { |team| team.name == 'Bears' }.first.score.should eql 3
      end

      it "should calculate 1 point per tie" do
        @teams.select { |team| team.name == "49ers" }.first.score.should eql 1
      end
    end

    describe '#sort_teams_by_standings' do
      it "should sort teams by score, descending" do
        awesome_team = @teams.select { |team| team.name == "Seahawks" }.first
        poor_team = @teams.select { |team| team.name == "Raiders" }.first

        @teams.index(awesome_team).should be < @teams.index(poor_team)
      end

      it "should sort teams with same score alphabetically" do
        lower_team = @teams.select { |team| team.name == "Apples" }.first
        upper_team = @teams.select { |team| team.name == "Cats" }.first

        @teams.index(lower_team).should be < @teams.index(upper_team)
      end
    end
  end
end
