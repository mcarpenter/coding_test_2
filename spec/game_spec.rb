require 'spec_helper.rb'
module SoccerStandings
  describe Game do
    before :each do
      @game = Game.new("Seahawks 58, Cardinals 0")
    end

    describe "#new" do
      it "takes one parameter and returns a Game object" do
        @game.should be_an_instance_of Game
      end
    end

    describe "#team_a_name" do
      it "should return the correct name" do
        @game.team_a_name.should eql "Seahawks"
      end
    end

    describe "#team_a_score" do
      it "should return the correct sore" do
        @game.team_a_score.should eql "58"
      end
    end

    describe "#team_b_name" do
      it "should return the correct name" do
        @game.team_b_name.should eql "Cardinals"
      end
    end

    describe "#team_b_score" do
      it "should return the correct score" do
        @game.team_b_score.should eql "0"
      end
    end
  end
end
