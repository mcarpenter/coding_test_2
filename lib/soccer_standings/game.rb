module SoccerStandings
  class Game
    attr_accessor :team_a_name,
                  :team_a_score,
                  :team_b_name,
                  :team_b_score

    # Parses a single game score string and instantiates a Game object
    #
    # @param [String] game_score game score string to be parsed
    # @return [Game] a Game instance containing the the names and scores of the teams that played
    def initialize(game_score)
      game_score.scan(/(.+?) ([0-9]+), (.+?) ([0-9]+)/) do |tan, tas, tbn, tbs|
        @team_a_name = tan
        @team_a_score = tas
        @team_b_name = tbn
        @team_b_score = tbs
      end
    end

    # Update the wins and ties for both teams that played in this game
    #
    # @param [Team] team_a first team that played in this game
    # @param [Team] team_b scond team that played in this game
    def update_team_records(team_a, team_b)
      if @team_a_score > @team_b_score
        team_a.record_win
      elsif @team_a_score < @team_b_score
        team_b.record_win
      else
        team_a.record_tie
        team_b.record_tie
      end
    end
  end
end
