module SoccerStandings
  class Team
    attr_accessor :name,
                  :wins,
                  :losses,
                  :ties,
                  :score
    # Instantiates a new Team object
    #
    # params [String] name name of team
    # return [Team] new Team object
    def initialize(name)
      @name = name
      @wins = 0
      @ties = 0
      @score = 0
    end

    # Returns a hash of Team objects keyed by team name from an array of games
    #
    # params [Array] games array of Game objects
    # return [Hash] hash of Team objects, keyed by teams names
    def self.from_game_array(games)
      teams = {}

      # Enumerate each game, check if the team already exists in
      # our hash of teams (or create new Team instance if not) then
      # update the teams hash using the team name as a key
      games.each do |game|
        team_a = get_team_by_name(game.team_a_name,teams)
        team_b = get_team_by_name(game.team_b_name,teams)

        teams[team_a.name] = team_a
        teams[team_b.name] = team_b
        
        # Calculate wins and ties for this game then save to the appropriate teams
        game.update_team_records(team_a, team_b)
      end
      return teams
    end

    # Finds Team object by name in hash of Teams then returns Team object
    #
    # params [String] team_name name of team
    # params [Hash] teams hash containing teams, keyed by team names
    # return [Team] matching Team object
    def self.get_team_by_name(team_name, teams)
      teams.each do |key, value|
        return value unless key != team_name
      end
      return Team.new(team_name)
    end
    
    # Records a win
    #
    # return [Integer] team wins
    def record_win
      @wins += 1
    end
    
    # Records a tie
    #
    # return [Integer] team ties
    def record_tie
      @ties += 1
    end
  end
end
