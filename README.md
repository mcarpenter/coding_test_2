Running This Solution
==========================================

Build and install the gem

	gem build soccer_standings.gemspec
	gem install soccer_standings-0.0.1.gem

Then run

	soccer_standings < sample-input.txt
	
----

Thank you for your interest in Substantial.
===========================================

This is a small problem that we would like you to solve so that we can get an idea of your coding ability.

What we look for
----------------
We will be looking at the quality and professionalism of your work. In particular we look for clean, well-designed, maintainable code. Although this is a rather small task, it should be approached as you would an actual task for a customer.

When you are finished
----------------------
Please push your solution to the provided github repository.

The Problem
===========
We want you to create a command-line application that will calculate the ranking table for a soccer league.

Input/output
------------
The input and output will be text. Either using stdin/stdout or taking filenames on the command line is fine.

The input contains results of games, one per line. See sample-input.txt for details.
The output should be ordered from most to least points, following the format specified in expected-output.txt.

You can expect that the input will be well-formed. There is no need to add special handling for malformed input files.

The rules
---------
In this league, a draw (tie) is worth 1 point and a win is worth 3 points. A loss is worth 0 points.
If two or more teams have the same number of points, they should have the same rank and be printed in alphabetical order (as in the tie for 3rd place in the sample data).

Guidelines
-----------
This should be implemented in a language with which you are familiar.
We would prefer that you use ruby, coffeescript, javascript, or python (in that order), if you are comfortable doing so.

If you use other libraries installed by a common package manager (rubygems/bundler, npm, pip), it is not necessary to commit the installed packages.

We write automated tests and we would like you to do so as well.

If there are any complicated setup steps necessary to run your solution, please document them.

Platform support
----------------
This will be run in a unix-ish environment (OS X). Please use platform-agnostic constructs where possible (line-endings and file-path-separators are two problematic areas).

Questions?
----------
Please email jobs+coding-test@substantial.com if you have any questions.
